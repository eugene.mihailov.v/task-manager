package ru.mihailov.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.event.CommandEvent;
import ru.mihailov.se.error.CommandCorruptException;

import java.util.*;

/**
 * @author Denis Volnenko
 */
@Component
public final class Bootstrap {

    @Autowired
    private ApplicationEventPublisher publisher;

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Scanner scanner = new Scanner(System.in);

    public void registry(final AbstractCommand command) {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        publisher.publishEvent(new CommandEvent(this, abstractCommand.command()));
    }

    public List<AbstractCommand> getListCommand() {
        return new ArrayList<>(commands.values());
    }

    public Optional<String> nextLine() {
        return Optional.of(scanner.nextLine());
    }

    public Optional<Integer> nextInteger() {
        final Optional<String> value = nextLine();

        if (!value.isPresent()) return Optional.empty();

        try {
            return Optional.of(Integer.parseInt(value.get()));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

}
