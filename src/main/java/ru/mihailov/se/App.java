package ru.mihailov.se;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.mihailov.se.config.AppConfig;
import ru.mihailov.se.controller.Bootstrap;

public class App {

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        final Bootstrap bootstrap = applicationContext.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
