package ru.mihailov.se.event;

import org.springframework.context.ApplicationEvent;

/**
 * Command execute event.
 */
public class CommandEvent extends ApplicationEvent {

    private String command;

    public CommandEvent(Object source, String command) {
        super(source);
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}
