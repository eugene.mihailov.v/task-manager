package ru.mihailov.se.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mihailov.se.entity.Project;

/**
 * @author Denis Volnenko
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}
