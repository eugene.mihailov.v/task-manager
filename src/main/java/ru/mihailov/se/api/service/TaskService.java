package ru.mihailov.se.api.service;

import ru.mihailov.se.entity.Task;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface TaskService {

    Task createTask(String name);

    Task getTaskById(String id);

    Task merge(Task task);

    void removeTaskById(String id);

    List<Task> getListTask();

    void clear();

    Task createTaskByProject(String projectId, String taskName);

}
