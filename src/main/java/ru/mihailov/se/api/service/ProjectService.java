package ru.mihailov.se.api.service;

import org.springframework.stereotype.Service;
import ru.mihailov.se.entity.Project;

import java.util.Collection;
import java.util.List;

@Service
public interface ProjectService {

    Project createProject(String name);

    Project merge(Project project);

    Project getProjectById(String id);

    void removeProjectById(String id);

    List<Project> getListProject();

    void clear();

    void merge(Project... projects);

}
