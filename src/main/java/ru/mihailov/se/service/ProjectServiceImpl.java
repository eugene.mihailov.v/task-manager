package ru.mihailov.se.service;

import java.util.Arrays;
import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import ru.mihailov.se.api.repository.ProjectRepository;
import ru.mihailov.se.api.service.ProjectService;
import ru.mihailov.se.entity.Project;

import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(final ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Transactional
    @Override
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = new Project();
        project.setName(name);
        Date now = new Date();
        project.setCreated(now);
        project.setDateBegin(now);
        return projectRepository.save(project);
    }

    @Transactional
    @Override
    public Project merge(final Project project) {
        return projectRepository.save(project);
    }

    @Override
    public Project getProjectById(final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Transactional
    @Override
    public void removeProjectById(final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    public List<Project> getListProject() {
        return projectRepository.findAll();
    }

    @Transactional
    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    @Override
    public void merge(Project... projects) {
        projectRepository.saveAll(Arrays.asList(projects));
    }

}
