package ru.mihailov.se.service;

import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import ru.mihailov.se.api.repository.ProjectRepository;
import ru.mihailov.se.api.repository.TaskRepository;
import ru.mihailov.se.api.service.TaskService;
import ru.mihailov.se.entity.Project;
import ru.mihailov.se.entity.Task;

import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public final class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    private final ProjectRepository projectRepository;

    public TaskServiceImpl(
            final TaskRepository taskRepository,
            final ProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Transactional
    @Override
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        Task task = new Task();
        task.setName(name);
        Date now = new Date();
        task.setDateBegin(now);
        return taskRepository.save(task);
    }

    @Override
    public Task getTaskById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Transactional
    @Override
    public Task merge(final Task task) {
        return taskRepository.save(task);
    }

    @Transactional
    @Override
    public void removeTaskById(final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @Transactional
    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

    @Transactional
    @Override
    public Task createTaskByProject(final String projectId, final String taskName) {
        final Project project = projectRepository.findById(projectId).orElse(null);
        if (project == null) return null;
        final Task task = createTask(taskName);
        task.setProjectId(project.getId());
        return task;
    }

}
