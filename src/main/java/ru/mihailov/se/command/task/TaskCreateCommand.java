package ru.mihailov.se.command.task;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mihailov.se.api.service.TaskService;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskCreateCommand extends AbstractCommand {

    private static final String COMMAND = "task-create";

    @Autowired
    private TaskService taskService;

    public TaskCreateCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Async
    @Override
    @EventListener(condition = "#event.command == @taskCreateCommand.command()")
    public void handler(CommandEvent event) {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");

        final Optional<String> name = bootstrap.nextLine();

        if (name.isPresent()) {
            taskService.createTask(name.get());
            System.out.println("[OK]");
        } else {
            System.out.println("Error! Incorrect name");
        }

        System.out.println();
    }

}
