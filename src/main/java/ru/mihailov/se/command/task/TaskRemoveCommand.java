package ru.mihailov.se.command.task;

import java.util.Optional;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskRemoveCommand extends AbstractCommand {

    private static final String COMMAND = "task-remove";

    public TaskRemoveCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Async
    @Override
    @EventListener(condition = "#event.command == '" + COMMAND + "'")
    public void handler(CommandEvent event) {
        System.out.println("[REMOVING TASK]");
        System.out.println("Enter task order index:");

        final Optional<Integer> orderIndex = bootstrap.nextInteger();

        if (!orderIndex.isPresent()) {
            System.out.println("Error! Incorrect order index...");
            System.out.println();
            return;
        }

        System.out.println("[OK]");
    }

}
