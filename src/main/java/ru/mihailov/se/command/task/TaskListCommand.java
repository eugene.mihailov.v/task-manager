package ru.mihailov.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mihailov.se.api.service.TaskService;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.entity.Task;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskListCommand extends AbstractCommand<Task> {

    private static final String COMMAND = "task-list";

    @Autowired
    private TaskService taskService;

    public TaskListCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Async
    @Override
    @EventListener(condition = "#event.command == '" + COMMAND + "'")
    public void handler(CommandEvent event) {
        System.out.println("[TASK LIST]");

        showList(taskService.getListTask(), Task::getName);

        System.out.println();
    }

}
