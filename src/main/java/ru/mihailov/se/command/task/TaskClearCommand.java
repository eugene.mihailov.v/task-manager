package ru.mihailov.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.mihailov.se.api.service.TaskService;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class TaskClearCommand extends AbstractCommand {

    private static final String COMMAND = "task-clear";

    @Autowired
    private TaskService taskService;

    public TaskClearCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Async
    @Override
    @EventListener(condition = "#event.command == '" + COMMAND + "'")
    public void handler(CommandEvent event) {
        taskService.clear();
        System.out.println("[ALL TASK REMOVED]");
    }

}
