package ru.mihailov.se.command.system;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class HelpCommand extends AbstractCommand {

    private static final String COMMAND = "help";

    public HelpCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    @EventListener(condition = "#event.command == @helpCommand.command()")
    public void handler(CommandEvent event) {
        for (AbstractCommand command: bootstrap.getListCommand()) {
            System.out.println(command.command()+ ": " + command.description());
        }
    }

}
