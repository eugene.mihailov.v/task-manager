package ru.mihailov.se.command;

import java.util.List;
import java.util.function.Function;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
public abstract class AbstractCommand<T> {

    protected Bootstrap bootstrap;

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract void handler(CommandEvent event) throws Exception;

    public abstract String command();

    public abstract String description();

    protected void showList(List<T> list, Function<T, String> f) {
        int[] index = new int[1];
        index[0] = 1;

        list.stream().forEach(item -> System.out.println(index[0]++ + ". " + f.apply(item)));
    }

}
