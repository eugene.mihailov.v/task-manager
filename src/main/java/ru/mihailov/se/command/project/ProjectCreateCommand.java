package ru.mihailov.se.command.project;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mihailov.se.api.service.ProjectService;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class ProjectCreateCommand extends AbstractCommand {

    private static final String COMMAND = "project-create";

    @Autowired
    private ProjectService projectService;

    public ProjectCreateCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    @EventListener(condition = "#event.command == '" + COMMAND + "'")
    public void handler(CommandEvent event) {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");

        final Optional<String> name = bootstrap.nextLine();

        if (name.isPresent()) {
            projectService.createProject(name.get());
            System.out.println("[OK]");
        } else {
            System.out.println("Error! Incorrect name");
        }

        System.out.println();
    }

}
