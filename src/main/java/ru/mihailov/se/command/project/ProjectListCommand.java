package ru.mihailov.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mihailov.se.api.service.ProjectService;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.entity.Project;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class ProjectListCommand extends AbstractCommand<Project> {

    private static final String COMMAND = "project-list";

    @Autowired
    private ProjectService projectService;

    public ProjectListCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    @EventListener(condition = "#event.command == '" + COMMAND + "'")
    public void handler(CommandEvent event) {
        System.out.println("[PROJECT LIST]");

        showList(projectService.getListProject(), Project::getName);

        System.out.println();
    }

}
