package ru.mihailov.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mihailov.se.api.service.ProjectService;
import ru.mihailov.se.command.AbstractCommand;
import ru.mihailov.se.controller.Bootstrap;
import ru.mihailov.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component
public class ProjectClearCommand extends AbstractCommand {

    private static final String COMMAND = "project-clear";

    @Autowired
    private ProjectService projectService;

    public ProjectClearCommand(Bootstrap bootstrap) {
        bootstrap.registry(this);
    }

    @Override
    public String command() {
        return COMMAND;
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    @EventListener(condition = "#event.command == '" + COMMAND + "'")
    public void handler(CommandEvent event) {
        projectService.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

}
